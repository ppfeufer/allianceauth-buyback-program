# Includes all asteroids, moon ore and ice. All items that can be compressed or refined for minerals*
ORE_EVE_GROUPS = [
    450,  # Arkonor
    451,  # Bistot
    452,  # Crokite
    453,  # Ochre
    454,  # Hedbergite
    455,  # Hemorphite
    456,  # Jaspet
    457,  # Kernite
    458,  # Plagioclase
    459,  # Pyroxeres
    460,  # Scordite
    461,  # Spodumain
    462,  # Veldspar
    465,  # Ice
    467,  # Gneiss
    468,  # Mercoxit
    469,  # Omber
    1884,  # Zeolites
    1911,  # Raspite
    1920,  # Cobaltite
    1921,  # Otavite
    1922,  # Carnotite
    1923,  # Xenotime
    2024,  # Amethystic Crystallite
    2029,  # ??
    4029,  # Talassonite
    4030,  # Rakovene
    4031,  # Bezdnacine
    4094,  # ??
    4513,  # Mordunium
    4514,  # Ytirium
    4515,  # Eifyrium
    4516,  # Ducinium
    4759,  # Griemeer
    4758,  # Hezorime
    4755,  # Kylixium
    4756,  # Nocxite
    4757,  # Ueganite
]

OPE_EVE_GROUPS = [493]

BONDS_EVE_GROUPS = [1248]

BLUE_LOOT_TYPE_IDS = [
    30744,
    30745,
    30746,
    30747,
]

RED_LOOT_TYPE_IDS = [
    48121,
    60459,
]
